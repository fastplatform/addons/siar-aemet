#!/bin/bash

git_rev=$(git rev-parse HEAD 2>/dev/null)
echo "BUILD_SCM_REVISION ${git_rev}"

git diff-index --quiet HEAD -- 2>/dev/null
if [[ $? == 0 ]];
then
    tree_status="Clean"
else
    tree_status="Modified"
fi
echo "BUILD_SCM_STATUS ${tree_status}"

GITSHA=$(git describe --always 2>/dev/null)

SIAR_AEMET_VERSION=${SIAR_AEMET_VERSION:=$(grep 'SIAR_AEMET_VERSION\s*=' VERSION | awk '{print $3}')}
SIAR_AEMET_CI_VERSION=${SIAR_AEMET_CI_VERSION:=$(grep 'SIAR_AEMET_CI_VERSION\s*=' VERSION | awk '{print $3}')}

if [[ -z "${VERSION}" ]]; then
  if [[ -z "${CI}" ]]; then
    VERSION=${SIAR_AEMET_VERSION}
  else
    VERSION="${SIAR_AEMET_CI_VERSION}+${GITSHA}"
  fi
fi

echo "STABLE_SIAR_AEMET_VERSION ${VERSION}"

SIAR_AEMET_TAG=${VERSION/+/-}
echo "STABLE_SIAR_AEMET_TAG ${SIAR_AEMET_TAG}"
echo "STABLE_SIAR_AEMET_TAG_PREFIXED_WITH_COLON :${SIAR_AEMET_TAG}"

if [[ -z "${DOCKER_REGISTRY}" ]]; then
  DOCKER_REGISTRY="pwcfasteu.azurecr.io"
fi
if [[ -z "${DOCKER_IMAGE_PREFIX}" ]]; then
  DOCKER_IMAGE_PREFIX=fastplatform/addons/siar-aemet/
fi
echo "STABLE_DOCKER_REGISTRY ${DOCKER_REGISTRY}"
echo "STABLE_DOCKER_IMAGE_PREFIX ${DOCKER_IMAGE_PREFIX}"
