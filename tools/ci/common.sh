#!/bin/bash

export CI=${CI:-}
export CI_BASE_DOMAIN=${CI_BASE_DOMAIN:-${KUBE_INGRESS_BASE_DOMAIN:-ci-review-domain-not-set}}
export CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG:-ci-commit-ref-slug-not-set}
export CI_ENVIRONMENT_DIR_SETTINGS=${CI_ENVIRONMENT_DIR_SETTINGS:-$(dirname $0)/envs/ephemeral-review}
export CI_ENVIRONMENT_INITIALIZATION_SCRIPT=${CI_ENVIRONMENT_INITIALIZATION_SCRIPT:-}
export CI_ENVIRONMENT_SLUG=${CI_ENVIRONMENT_SLUG:-ci-environment-slug-not-set}
export CI_ENVIRONMENT_SHORT_SLUG=${CI_ENVIRONMENT_SHORT_SLUG:-${CI_ENVIRONMENT_SLUG:(-3)}} # ^envname-[a-zA-Z0-9]{6}$$
export CI_ENVIRONMENT_DOMAIN=${CI_ENVIRONMENT_DOMAIN:-${CI_ENVIRONMENT_SHORT_SLUG}.r.${CI_BASE_DOMAIN}}
export CI_JOB_TOKEN=${CI_JOB_TOKEN:-}
export CI_PROJECT_PATH=${CI_PROJECT_PATH:-ci-project-path-not-set}
export CI_REGISTRY=${CI_REGISTRY:-ci-registry-not-set.fastplatform.eu}

export KUBE_NAMESPACE=${KUBE_NAMESPACE:-$CI_ENVIRONMENT_SLUG} # KUBE_NAMESPACE is a GitLab env variable
export NAMESPACE=${NAMESPACE:-$KUBE_NAMESPACE}
export DOCKER_REGISTRY=${DOCKER_REGISTRY:=$CI_REGISTRY}
