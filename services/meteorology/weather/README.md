# meteorology/weather

```meteorology/weather``` is a [FastAPI](https://fastapi.tiangolo.com/) (+ [Graphene](https://graphene-python.org/) framework) service that provides climatic and weather observations and forecasts, and is based on public data exposed by [SiAR web API (v1)](https://opendata.aemet.es/) and [AEMET HATEOAS API](https://servicio.mapama.gob.es).

It exposes 3 types of objects through a single GraphQL endpoint:
- **weather observations (past data)**: this data is fetched from the SiAR API, at a weather station level
- **weather forecasts**: fetched from the AEMET Open Data API, at the municipality/muncipio level
- **weather alerts**: fetched from the AEMET Open Data API, at the country level and geographically indexed on the fly

This service therefore exposes a GraphQL ontology that can be natively federated at a higher level into another GraphQL ontology. It instantly enables the modular aspect of SIAR-AMET's custom logic. The exposed GraphQL schema is compliant with the [FaST Weather GraphQL ontology](https://gitlab.com/fastplatform/pypi/fastplatform/-/blob/master/fastplatform-graphql/fastplatform_graphql/types/weather.py).

The service embarks the shapefile of all Spanish municipios, which is loaded in memory and geographically indexed when the service starts.

## Dependencies

The service builds upon the [SiAR web API (v1)](https://opendata.aemet.es/) and [AEMET HATEOAS API](https://servicio.mapama.gob.es).

## Prerequisites

- Python 3.7+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)
- A SiAR API key to retrieve at http://eportal.mapa.gob.es/websiar/Inicio.aspx and set in [secrets.env](secrets.env) file (cf. SIAR_API_KEY environment variable).
- An AEMET API key to retrieve at https://opendata.aemet.es/centrodedescargas/inicio and set in [secrets.env](secrets.env) file (cf. AEMET_API_KEY environment variable).

## Environment variables

- `REDIS_HOST`: URL of the redis server
- `REDIS_PORT`: port of the redis server
- `REDIS_PASSWORD`: password of the redis server if any
- `TIMEZONE`: timezone of the server, defaults to `"Europe/Madrid"`
- `SIAR_STATION_CACHE_EXPIRE`: expiration of the cache of SiAR stations, defaults to `24 * 60 * 60` seconds (1 day)
- `SIAR_HOURLY_OBSERVATIONS_CACHE_EXPIRE`: expiration of the cache for hourly observations, defaults to `24 * 60 * 60` seconds (1 day)
- `SIAR_DAILY_OBSERVATION_CACHE_EXPIRE`: expiration of the cache for daily observations, defaults to `24 * 60 * 60` seconds (1 day)
- `SIAR_HOURLY_OBSERVATIONS_TODAY_CACHE_EXPIRE`: expiration of the cache for hourly observations *of the current day*, defaults to `30 * 60` seconds (30 minutes)
- `SIAR_DAILY_OBSERVATION_TODAY_CACHE_EXPIRE`: expiration of the cache for daily observations *of the current day*, defaults to `30 * 60` seconds (30 minutes)
- `SIAR_API_KEY`: API key to access the SiAR data, *no default value*
- `SIAR_API_SRID`: SRID of the coordinatees returned by the SiAR APIs, defaults to `4258` (ETRS89)
- `SIAR_API_STATIONS_URL`: URL of the SiAR weather stations API, defaults to `"https://servicio.mapama.gob.es/apisiar/API/v1/info/Estaciones"`
- `SIAR_API_HOURLY_OBSERVATIONS_URL`: URL of the SiAR hourly observations API, defaults to `"https://servicio.mapama.gob.es/apisiar/API/v1/datos/Horarios/Estacion"`
- `SIAR_API_DAILY_OBSERVATION_URL`: URL of the SiAR daily observations API, defaults to `"https://servicio.mapama.gob.es/apisiar/API/v1/datos/Diarios/Estacion"`
- `AEMET_FORECAST_CACHE_EXPIRE`: expiration of the cache for AEMET forecasts, defaults to `10 * 60` seconds (10 minutes)
- `AEMET_ALERTS_CACHE_EXPIRE`: expiration of the cache for AEMET alerts, defaults to `10 * 60` seconds (10 minutes)
- `AEMET_API_KEY`: API key to access the SiAR data, *no default value*
- `AEMET_API_ALERTS_URL`: URL of the AEMET alerts API for Spain, defaults to `"https://opendata.aemet.es/opendata/api/avisos_cap/ultimoelaborado/area/esp"`

## Development Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

If needed start monitoring OpenTelemetry traces
```
make start-tracing
```
Jaeger UI will be available at [http://localhost:16686](http://localhost:16686)

Start the service:
```bash
make start_redis # start a local Redis datastore
make start
```

The API server is now started and available at [http://localhost:7777/graphql](http://localhost:7777/graphql), with live reload enabled.

**Server can be also started with Bazel** (no need to activate a Python virtualenv):
```bash
$ make bazel-run
```

## Sample 

A sample (full) GraphQL query is included in the [tests](tests) folder.
