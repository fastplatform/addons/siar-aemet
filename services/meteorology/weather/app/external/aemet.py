from datetime import datetime, timedelta
import csv
import os
import pickle
import math
import itertools

import fiona
import pytz
import requests
import tempfile
import tarfile
import io
import json
import redis
import dateutil.parser
from capparselib.parsers import CAPParser
from aemet import Aemet, PERIODO_DIA, PERIODO_SEMANA, Prediccion
from shapely.geometry import shape
from shapely.strtree import STRtree


TZ = pytz.utc
DATA_DIR = os.path.join(
    os.path.dirname(__file__),
    "../../data",
)

WIND_DIR_TO_BEARING = {
    "N": 0,
    "NE": 45,
    "E": 90,
    "SE": 135,
    "S": 180,
    "SO": 225,
    "O": 270,
    "NO": 315,
    "C": None,
}


class AEMETCache:
    def __init__(self):
        self.redis_client = redis.Redis(
            host=os.environ.get("REDIS_HOST"),
            port=os.environ.get("REDIS_PORT"),
            password=os.environ.get("REDIS_PASSWORD"),
            db=0,
        )

        self.forecast_prefix = "AEMET_FORECAST"
        self.alerts_prefix = "AEME_ALERT"
        self.forecast_expire = os.environ.get("AEMET_FORECAST_CACHE_EXPIRE", 10 * 60)
        self.alerts_expire = os.environ.get("AEMET_ALERTS_CACHE_EXPIRE", 10 * 60)

    def get_forecast(self, municipio_id):
        forecast = self.redis_client.get(f"{self.forecast_prefix}|{municipio_id}")
        if forecast:
            forecast = pickle.loads(forecast)
            return forecast
        return None

    def write_forecast(self, municipio_id, forecast):
        self.redis_client.setex(
            f"{self.forecast_prefix}|{municipio_id}",
            self.forecast_expire,
            pickle.dumps(forecast),
        )

    def get_alerts(self):
        alerts = self.redis_client.get(f"{self.alerts_prefix}")
        if alerts:
            alerts = pickle.loads(alerts)
            return alerts
        return None

    def write_alerts(self, alerts):
        self.redis_client.setex(
            f"{self.alerts_prefix}",
            self.alerts_expire,
            pickle.dumps(alerts),
        )

    def has_alerts(self):
        return self.redis_client.exists(f"{self.alerts_prefix}")


class AEMETClient:
    def __init__(self, cache):
        self.cache = cache
        self.api_key = os.environ.get("AEMET_API_KEY")
        self.aemet = Aemet(api_key=self.api_key)

        self.alerts_url = os.environ.get(
            "AEMET_API_ALERTS_URL",
            "https://opendata.aemet.es/opendata/api/avisos_cap/ultimoelaborado/area/esp",
        )

        self.icons_mapping = {}
        aemet_codes_file = os.path.join(
            DATA_DIR,
            "aemet-codes.csv",
        )
        with open(aemet_codes_file, "r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.icons_mapping[row["AEMET_CODE"]] = row["FAST_CODE"]

        self.bearing_mapping = {
            "N": 0,
            "NE": 45,
            "E": 90,
            "SE": 135,
            "S": 180,
            "SO": 225,
            "O": 270,
            "NO": 315,
            "C": None,
        }

        self.layers = {
            "municipios": {"tree": None, "index": None},
            "provincias": {"tree": None, "index": None},
            "ccaa": {"tree": None, "index": None},
        }
        self.alerts = None
        self.alerts_tree = None

    def load_layers(self):
        for layer in ["municipios", "provincias", "ccaa"]:
            layer_file = os.path.join(
                DATA_DIR,
                f"shapefiles/{layer}.shp",
            )
            data = fiona.open(layer_file, "r")
            shapes = [shape(f["geometry"]) for f in data]
            self.layers[layer]["tree"] = STRtree(shapes)
            self.layers[layer]["index"] = dict((id(s), f) for s, f in zip(shapes, data))

    def get_max_intersection_from_geometry(self, layer, geometry):
        """Find the polygon with the biggest intersection with this geometry

        Arguments:
            layer {str} -- The layer to check ('municipios', 'provincias', 'ccaa')
            geometry {dict} -- The geometry

        Returns:
            dict -- The feature with the biggest intersection
        """
        geometry = shape(geometry)

        # Memory IDs of shapes that intersect the geometry
        shape_ids = [id(s) for s in self.layers[layer]["tree"].query(geometry)]

        # Features corresponding to these memory IDs, as per the index
        features = [self.layers[layer]["index"][shape_id] for shape_id in shape_ids]

        # Find max intersection
        intersection_areas = [
            shape(f["geometry"]).intersection(geometry).area for f in features
        ]
        index_intersection_area_max = max(
            range(len(intersection_areas)), key=lambda i: intersection_areas[i]
        )

        return features[index_intersection_area_max]

    def get_forecast_for_municipio(self, municipio, interval_type):
        municipio_id = municipio["properties"]["NATCODE"][-5:]
        forecast = self.cache.get_forecast(municipio_id)
        if forecast is None:
            forecast = self.fetch_forecast_for_municipio(municipio)
            self.cache.write_forecast(municipio_id, forecast)
        return forecast[interval_type]

    def fetch_forecast_for_municipio(self, municipio):
        """Fetch hourly forecast for municipality and write it the the database

        Arguments:
            municipality {str} -- Municipality feature from the shapefile
        """
        municipio_id = municipio["properties"]["NATCODE"][-5:]

        hourly_forecasts_raw = self.aemet.get_prediccion(
            municipio_id, periodo=PERIODO_DIA, raw=True
        )
        hourly_forecasts = Prediccion.from_json(
            hourly_forecasts_raw, periodo=PERIODO_DIA
        )

        daily_forecasts_raw = self.aemet.get_prediccion(
            municipio_id, periodo=PERIODO_SEMANA, raw=True
        )
        daily_forecasts = Prediccion.from_json(
            daily_forecasts_raw, periodo=PERIODO_SEMANA
        )

        fetched_at = datetime.now(tz=TZ)

        hourly_forecasts = self.parse_hourly_forecasts(
            hourly_forecasts, municipio_id, municipio, fetched_at
        )
        daily_forecasts = self.parse_daily_forecasts(
            daily_forecasts, municipio_id, municipio, fetched_at
        )

        return {"hour": hourly_forecasts, "day": daily_forecasts}

    def parse_hourly_forecasts(
        self, forecast: Prediccion, municipio_id, municipio, fetched_at
    ):
        data = {}

        computed_at = TZ.localize(
            datetime.strptime(forecast.elaborado, "%Y-%m-%dT%H:%M:%S")
        )

        origin = {
            "name": forecast.origen["productor"],
            "url": forecast.origen["web"],
            "language": "es-es",
        }

        for day in forecast.prediccion:
            day_date = TZ.localize(datetime.strptime(day.fecha, "%Y-%m-%dT%H:%M:%S"))

            for sample in day.estadoCielo:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    data[valid_from] = {
                        "summary": sample["descripcion"],
                        "icon": self.icons_mapping.get(
                            sample.get("value", None), "other"
                        ),
                    }

            for sample in day.humedadRelativa:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    if valid_from not in data:
                        data[valid_from] = {}
                    data[valid_from]["humidity"] = (
                        float(sample.get("value", None) or 0) / 100
                    )

            for sample in day.precipitacion:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    if valid_from not in data:
                        data[valid_from] = {}
                    if sample.get("value", None) in ["Ip", None]:
                        data[valid_from]["precipitation_intensity"] = 0
                    else:
                        data[valid_from]["precipitation_intensity"] = float(
                            sample["value"]
                        )
                        data[valid_from]["precipitation_type"] = "rain"

            for sample in day.probPrecipitacion:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    if valid_from not in data:
                        data[valid_from] = {}
                    data[valid_from]["precipitation_probability"] = (
                        float(sample.get("value", None) or 0) / 100
                    )
                    if float(sample.get("value", None) or 0) > 0:
                        data[valid_from]["precipitation_type"] = "rain"

            for sample in day.probTormenta:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    if valid_from not in data:
                        data[valid_from] = {}
                    # Override the rain values if there is storm instead
                    if float(sample.get("value", None) or 0) > 0:
                        data[valid_from]["precipitation_probability"] = (
                            float(sample.get("value", None) or 0) / 100
                        )
                        data[valid_from]["precipitation_type"] = "storm"

            for sample in day.nieve:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    if valid_from not in data:
                        data[valid_from] = {}
                    # Override the rain values if there is snow instead
                    if float(sample.get("value", None) or 0) > 0:
                        data[valid_from]["precipitation_intensity"] = float(
                            sample.get("value", None) or 0
                        )
                        data[valid_from]["precipitation_type"] = "snow"

            for sample in day.probNieve:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    if valid_from not in data:
                        data[valid_from] = {}
                    # Override the rain values if there is snow instead
                    if float(sample.get("value", None) or 0) > 0:
                        data[valid_from]["precipitation_probability"] = (
                            float(sample.get("value", None) or 0) / 100
                        )
                        data[valid_from]["precipitation_type"] = "snow"

            for sample in day.sensTermica:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    if valid_from not in data:
                        data[valid_from] = {}
                    data[valid_from]["apparent_temperature"] = float(
                        sample.get("value", None) or 0
                    )

            for sample in day.temperatura:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    if valid_from not in data:
                        data[valid_from] = {}
                    data[valid_from]["temperature"] = float(
                        sample.get("value", None) or 0
                    )

            for sample in day.vientoAndRachaMax:
                for hour in AEMETClient._hours_from_periodo(sample["periodo"]):
                    valid_from = day_date + timedelta(hours=hour)
                    if valid_from not in data:
                        data[valid_from] = {}
                    if "value" in sample:
                        data[valid_from]["wind_gust"] = float(
                            sample.get("value", None) or 0
                        )
                    elif (
                        "direccion" in sample
                        and "velocidad" in sample
                        and sample["direccion"]
                        and sample["velocidad"]
                    ):
                        bearings = [
                            self.bearing_mapping[d]
                            for d in sample["direccion"]
                            if self.bearing_mapping[d] is not None
                        ]
                        speeds = [float(v or 0) for v in sample["velocidad"]]
                        if bearings:
                            data[valid_from]["wind_bearing"] = sum(bearings) / len(
                                bearings
                            )
                        if speeds:
                            data[valid_from]["wind_speed"] = sum(speeds) / len(speeds)
                    else:
                        # Ignore if there is one wrong
                        pass

        geometry = shape(municipio["geometry"]).centroid.__geo_interface__
        geometry["crs"] = {"type": "name", "properties": {"name": "EPSG:4258"}}

        return [
            {
                "id": f'{municipio_id}|{valid_from.strftime("%Y-%m-%dT%H:%M:%S")}',
                "location": {
                    "id": municipio_id,
                    "name": municipio["properties"].get("NAMEUNIT", None),
                    "geometry": geometry,
                },
                "origin": origin,
                "computed_at": computed_at,
                "fetched_at": fetched_at,
                "valid_from": valid_from,
                "valid_to": valid_from + timedelta(hours=1),
                "sample_type": "forecast",
                "interval_type": "hour",
                "computed_at": computed_at,
                "fetched_at": fetched_at,
                "origin": origin,
                # Add only the fields that are not None
                **{k: v for k, v in data[valid_from].items() if v is not None},
            }
            for valid_from in data.keys()
        ]

    def parse_daily_forecasts(
        self, forecast: Prediccion, municipio_id, municipio, fetched_at
    ):
        data = {}

        computed_at = TZ.localize(
            datetime.strptime(forecast.elaborado, "%Y-%m-%dT%H:%M:%S")
        )

        origin = {
            "name": forecast.origen["productor"],
            "url": forecast.origen["web"],
            "language": "es-es",
        }

        for day in forecast.prediccion:
            day_date = TZ.localize(datetime.strptime(day.fecha, "%Y-%m-%dT%H:%M:%S"))

            # For wind gust, look for the max
            # (AEMET returns a lot of empty values like '')
            wind_gust = None
            for period in day.rachaMax:
                value = period.get("value", "")
                if value == "":
                    continue
                value = float(value)
                if wind_gust is None or value > wind_gust:
                    wind_gust = value

            # For apparent temperature, we keep only the max (there is no min/max
            # in the FaST weather model)
            apparent_temperature = day.sensTermica.get("maxima", None)

            # For max and min temperature and humidity, we just map to the AEMET values
            temperature_max = day.temperatura.get("maxima", None)
            temperature_min = day.temperatura.get("minima", None)

            humidities = list(map(lambda p: p["value"], day.humedadRelativa["dato"]))
            if humidities:
                humidity = sum(humidities) / len(humidities) / 100
            else:
                humidity = None
            humidity_max = day.humedadRelativa.get("maxima", None) / 100
            humidity_min = day.humedadRelativa.get("minima", None) / 100

            # For the icon, we first look for an icon for the full day, if there none, then
            # we take the one of the afternoon if there is one
            icon = None
            full_day = list(
                filter(
                    lambda p: ("periodo" not in p or p["periodo"] == "00-24")
                    and p["value"] != "",
                    day.estadoCielo,
                )
            )

            if full_day:
                icon = self.icons_mapping.get(full_day[0]["value"], "other")
            else:
                afternoon = list(
                    filter(
                        lambda p: p.get("periodo", None) in ["12-24", "12-18"]
                        and p["value"] != "",
                        day.estadoCielo,
                    )
                )
                if afternoon:
                    icon = self.icons_mapping.get(afternoon[0]["value"], "other")
            if icon is None:
                icon = "other"

            # For wind speeds, we use the average and max of the periods from AEMET
            periods = list(
                filter(
                    lambda p: p.get("periodo", "")
                    in ["00-06", "06-12", "12-18", "18-24", "00-12", "12-24", ""],
                    day.viento,
                )
            )
            if periods:
                wind_speeds = list(map(lambda p: float(p["velocidad"]), periods))
                wind_speed = 5 * round(
                    sum(wind_speeds) / len(wind_speeds) / 5
                )  # round to closest 5km/h
                wind_speed_max = max(wind_speeds)
            else:
                wind_speed = None
                wind_speed_max = None

            # For wind direction, we compute the angular average over the periods
            if periods:
                wind_directions = map(lambda p: p["direccion"], periods)
                wind_vectors = []
                for wind_direction in wind_directions:
                    wind_bearing = WIND_DIR_TO_BEARING.get(wind_direction, None)
                    if wind_bearing is None:
                        continue
                    wind_bearing_rad = math.radians(wind_bearing)
                    wind_vector = [
                        math.cos(wind_bearing_rad),
                        math.sin(wind_bearing_rad),
                    ]
                    wind_vectors.append(wind_vector)

                if wind_vectors:
                    has_divergent_bearings = False
                    for wind_vector_1, wind_vector_2 in itertools.product(
                        wind_vectors, wind_vectors
                    ):
                        dot_product = sum(
                            x * y for x, y in zip(wind_vector_1, wind_vector_2)
                        )
                        if dot_product < 0:
                            has_divergent_bearings = True
                            break
                    if has_divergent_bearings:
                        wind_bearing = None
                    else:
                        average_wind_vector = [
                            sum(v[0] for v in wind_vectors) / len(wind_vectors),
                            sum(v[1] for v in wind_vectors) / len(wind_vectors),
                        ]
                        wind_bearing = math.degrees(
                            math.atan2(average_wind_vector[1], average_wind_vector[0])
                        )
                        wind_bearing = 5 * round(wind_bearing / 5)  # round to closest 5 degrees
                else:
                    wind_bearing = None
            else:
                wind_bearing = None

            # For the precipitation probability, we just take the max
            precipitation_probability = max(
                map(lambda p: p["value"], day.probPrecipitacion), default=None
            )
            if precipitation_probability is not None and precipitation_probability > 0:
                precipitation_probability /= 100
                precipitation_type = "rain"
                if any([p["value"] != "" for p in day.cotaNieveProv]):
                    precipitation_type = "snow"
            else:
                precipitation_type = None

            data[day_date] = {
                "wind_gust": wind_gust,
                "apparent_temperature": apparent_temperature,
                "humidity_min": humidity_min,
                "humidity_max": humidity_max,
                "humidity": humidity,
                "temperature_min": temperature_min,
                "temperature_max": temperature_max,
                "icon": icon,
                "wind_speed": wind_speed,
                "wind_speed_max": wind_speed_max,
                "wind_bearing": wind_bearing,
                "precipitation_type": precipitation_type,
                "precipitation_probability": precipitation_probability,
            }

        geometry = shape(municipio["geometry"]).centroid.__geo_interface__
        geometry["crs"] = {"type": "name", "properties": {"name": "EPSG:4258"}}

        return [
            {
                "id": f'{municipio_id}|{valid_from.strftime("%Y-%m-%dT%H:%M:%S")}',
                "location": {
                    "id": municipio_id,
                    "name": municipio["properties"].get("NAMEUNIT", None),
                    "geometry": geometry,
                },
                "origin": origin,
                "computed_at": computed_at,
                "fetched_at": fetched_at,
                "valid_from": valid_from,
                "valid_to": valid_from + timedelta(hours=24),
                "sample_type": "forecast",
                "interval_type": "day",
                "computed_at": computed_at,
                "fetched_at": fetched_at,
                "origin": origin,
                # Add only the fields that are not None
                **data[valid_from],
            }
            for valid_from in data.keys()
        ]

    @staticmethod
    def _hours_from_periodo(periodo):
        if len(periodo) == 2:
            return [int(periodo)]
        elif len(periodo) == 4:
            start = int(periodo[:2])
            end = int(periodo[-2:])
            if end < start:
                end += 24
            return range(start, end)

    def get_alerts_for_geometry(self, geometry):
        geometry = shape(geometry)
        if not self.cache.has_alerts():
            alerts = self.fetch_alerts()
            self.cache.write_alerts(alerts)
            self.alerts = None
            self.alerts_tree = None

        if not self.alerts:
            alerts = self.cache.get_alerts()

            self.alerts = {a["id"]: a for a in alerts}
            polygons = []
            for a in alerts:
                polygon = shape(a["location"]["geometry"])
                polygon.alert_id = a["id"]
                polygons += [polygon]
            self.alerts_tree = STRtree(polygons)

        polygons = self.alerts_tree.query(geometry)
        alerts = [self.alerts[p.alert_id] for p in polygons]

        return alerts

    def fetch_alerts(self):

        response = requests.get(
            self.alerts_url,
            params={"api_key": self.api_key},
        )

        response = response.json()

        if "datos" not in response:
            raise Exception("Invalid response")

        caps = []

        archive_url = response["datos"]
        archive = requests.get(archive_url).content
        archive = io.BytesIO(archive)
        with tarfile.open(fileobj=archive, mode="r|") as tar:
            member = tar.next()
            while member:
                caps_xml = tar.extractfile(member).read().decode()
                caps += CAPParser(caps_xml).as_dict()
                member = tar.next()

        alerts = []

        for cap in caps:
            cap_info = next(i for i in cap["cap_info"] if i["cap_language"] == "es-ES")

            polygons = []
            for area in cap_info["cap_area"]:
                polygon = str(area["polygon"]).split(" ")
                polygon = [cc.split(",") for cc in polygon]
                polygon = [[float(cc[1]), float(cc[0])] for cc in polygon]
                polygons += [polygon]

            alert = {
                "id": str(cap.get("cap_id")),
                "sender": str(cap_info.get("cap_sender_name")),
                "issued_at": dateutil.parser.isoparse(str(cap.get("cap_sent"))),
                "title": str(cap_info.get("cap_headline")),
                "summmary": str(cap_info.get("cap_event")),
                "description": str(cap_info.get("cap_description")),
                "url": str(cap_info.get("cap_link")),
                "category": str(cap_info.get("cap_category")).lower(),
                "urgency": str(cap_info.get("cap_urgency")).lower(),
                "severity": str(cap_info.get("cap_severity")).lower(),
                "certainty": str(cap_info.get("cap_certainty")).lower(),
                "location": {"geometry": {"type": "Polygon", "coordinates": polygons}},
                "valid_from": dateutil.parser.isoparse(
                    str(cap_info.get("cap_effective"))
                ),
                "valid_to": dateutil.parser.isoparse(str(cap_info.get("cap_expires"))),
            }
            alerts += [alert]

        return alerts


aemet_cache = AEMETCache()
aemet_client = AEMETClient(cache=aemet_cache)
