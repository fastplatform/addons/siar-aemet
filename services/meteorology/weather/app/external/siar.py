import os
from datetime import date, datetime, timedelta
import pickle
import pytz
import redis
from operator import itemgetter
from itertools import groupby
from shapely.strtree import STRtree
from shapely.geometry import shape

import requests
from shapely.geometry import Point


class SiARCache:
    def __init__(self):

        self.redis_client = redis.Redis(
            host=os.environ.get("REDIS_HOST", "localhost"),
            port=os.environ.get("REDIS_PORT", 6379),
            password=os.environ.get("REDIS_PASSWORD"),
            db=0,
        )

        self.station_prefix = "SIAR_STATION"
        self.hourly_observations_prefix = "SIAR_HOURLY_OBSERVATIONS"
        self.daily_observation_prefix = "SIAR_DAILY_OBSERVATION"

        self.tz = pytz.utc

        self.station_expire = os.environ.get("SIAR_STATION_CACHE_EXPIRE", 24 * 60 * 60)

        self.hourly_observations_expire = os.environ.get(
            "SIAR_HOURLY_OBSERVATIONS_CACHE_EXPIRE", 24 * 60 * 60
        )
        self.daily_observation_expire = os.environ.get(
            "SIAR_DAILY_OBSERVATION_CACHE_EXPIRE", 24 * 60 * 60
        )

        self.hourly_observations_today_expire = os.environ.get(
            "SIAR_HOURLY_OBSERVATIONS_TODAY_CACHE_EXPIRE", 30 * 60
        )
        self.daily_observation_today_expire = os.environ.get(
            "SIAR_DAILY_OBSERVATION_TODAY_CACHE_EXPIRE", 30 * 60
        )

    def write_stations(self, stations):
        """Write stations to redis

        Arguments:
            stations {list} -- List of stations
        """
        pipeline = self.redis_client.pipeline()
        for s in stations:
            pipeline.setex(
                f'{self.station_prefix}|{s["id"]}', self.station_expire, pickle.dumps(s)
            )
        pipeline.execute()

    def get_stations(self):
        """Get stations from redis

        Returns:
            list -- List of stations
        """
        station_ids = self.redis_client.keys(f"{self.station_prefix}|*")
        stations = self.redis_client.mget(station_ids)

        if stations and stations[0] is not None:
            return [pickle.loads(s) for s in stations]
        else:
            return []

    def get_station(self, station_id):
        """Get a specific station from redis

        Arguments:
            station_id {str} -- SiAR ID of the station

        Returns:
            dict -- Station
        """
        station = self.redis_client.get(f"SIAR_STATION|{station_id}")
        if station:
            return pickle.loads(station)
        return None

    def get_date_ranges_to_fetch(self, prefix, station_id, date_from, date_to):
        """Split a date range into a list of ranges to fetch and a list of dates
        that are already in the cache for a given station and a given prefix (daily
        or hourly)

        For example if the request is for period 10/03/2020 to 26/03/2020 and the cache
        contains already data for 14/03/2020 to 16/03/2020, the function will return:
        - to fetch: [[10/03/2020, 13/03/2020],  [17/03/2020, 26/03/2020]]
        - in cache: [14/03/2020, 15/03/2020, 16/03/2020]

        Arguments:
            prefix {str} -- Prefix for hourly or daily data
            station_id {str} -- SiAR ID of the station
            date_from {date} -- Start of date range
            date_to {date} -- End of date range (included)

        Returns:
            tuple -- List of [start, end] dates to fetch, list of dates in cache
        """
        date_to = min(date_to, self.tz.localize(datetime.now()).date())

        # Build the list of requested dates, substract the ones that are already
        # in the cache
        requested_dates = [
            date_from + timedelta(days=d) for d in range((date_to - date_from).days + 1)
        ]
        existing_keys = self.redis_client.keys(f"{prefix}|{station_id}|*")
        existing_keys = [k.decode("utf-8") for k in existing_keys if k is not None]
        existing_dates = [
            datetime.strptime(d.split("|")[2], "%Y-%m-%d").date() for d in existing_keys
        ]
        missing_dates = [d for d in requested_dates if d not in existing_dates]

        # Group them by contiguous ranges
        to_fetch = []
        for key, group in groupby(
            enumerate(missing_dates), lambda t: t[1] - timedelta(days=t[0])
        ):
            group = list(group)
            if len(group) == 1:
                to_fetch += [[group[0][1], group[0][1]]]
            else:
                to_fetch += [[group[0][1], group[-1][1]]]

        return to_fetch, existing_dates

    def write_samples(self, prefix, station_id, date_from, date_to, samples):
        """Store samples/observations to cache

        Sets an expiry duration for samples of the current day, so that they are re-fetched from
        the API at a regular interval.

        Arguments:
            pipeline {redis.Pipeline} -- Redis pipeline to write instructions to
            prefix {str} -- Either 'SIAR_HOURLY_DATA' or 'SIAR_DAILY_DATA'
            station_id {str} -- SiAR station ID
            date_str {str} -- Date of the sample formatted as %Y-%m-%d
            samples {object} -- Sample(s) or list of samples
            today_expiry {int} -- Expiry duration for samples of current day
        """
        if prefix == self.hourly_observations_prefix:
            expiry = self.hourly_observations_expire
            expiry_today = self.hourly_observations_expire
        elif prefix == self.daily_observation_prefix:
            expiry = self.daily_observation_expire
            expiry_today = self.daily_observation_today_expire
        else:
            raise Exception('INVALID_PREFIX')

        pipeline = self.redis_client.pipeline()

        dates_str_with_samples = []
        for date_str, date_samples in groupby(
            samples, lambda s: s["valid_from"].strftime("%Y-%m-%d")
        ):
            dates_str_with_samples += [date_str]
            if date_str == self.tz.localize(datetime.now()).strftime("%Y-%m-%d"):
                expiry = expiry_today
            pipeline.setex(
                f"{prefix}|{station_id}|{date_str}",
                expiry,
                pickle.dumps(list(date_samples)),
            )

        dates_str_no_samples = [
            (date_from + timedelta(days=d)).strftime("%Y-%m-%d")
            for d in range((date_to - date_from).days + 1)
        ]
        dates_str_no_samples = [
            date_str
            for date_str in dates_str_no_samples
            if date_str not in dates_str_with_samples
        ]

        for date_str in dates_str_no_samples:
            if date_str == self.tz.localize(datetime.now()).strftime("%Y-%m-%d"):
                expiry = expiry_today
            pipeline.setex(
                f"{prefix}|{station_id}|{date_str}",
                expiry,
                pickle.dumps([]),
            )

        pipeline.execute()

    def get_samples(self, prefix, station_id, date_from, date_to):
        dates = [
            date_from + timedelta(days=d) for d in range((date_to - date_from).days + 1)
        ]
        keys = [f'{prefix}|{station_id}|{d.strftime("%Y-%m-%d")}' for d in dates]
        samples = self.redis_client.mget(*keys)
        if samples is None or samples[0] is None:
            return []
        else:
            return [pickle.loads(s) for s in samples]


class SiARClient:
    """Client class for managing the connection to the SiAR API and the local redis cache"""

    def __init__(self, cache):
        """Contructor

        Arguments:
            cache {SiAR} -- SiARCache instance
        """

        # We will store the weather stations (lat-long points) locally in an indexed RTree
        # for quick lookups on each request
        self.stations = None
        self.stations_index = None
        self.stations_rtree = None

        # SiARCache object to manages the connection to the redis cache
        self.cache = cache

        # Timezone of the SiAR API responses
        self.tz = pytz.utc

        # SiAR parameters
        self.api_key = os.environ.get("SIAR_API_KEY")
        self.api_srid = os.environ.get("SIAR_API_SRID", 4258)
        self.api_stations_url = os.environ.get(
            "SIAR_API_STATIONS_URL",
            "https://servicio.mapama.gob.es/apisiar/API/v1/info/Estaciones",
        )
        self.api_hourly_observations_url = os.environ.get(
            "SIAR_API_HOURLY_OBSERVATIONS_URL",
            "https://servicio.mapama.gob.es/apisiar/API/v1/datos/Horarios/Estacion",
        )
        self.api_daily_observation_url = os.environ.get(
            "SIAR_API_DAILY_OBSERVATION_URL",
            "https://servicio.mapama.gob.es/apisiar/API/v1/datos/Diarios/Estacion",
        )

    def load_stations(self):
        """Load and index the stations, either from the redis cache or from the SiAR API"""
        self.stations = self.cache.get_stations()
        if not self.stations:
            self.stations = self.fetch_stations()

        # Index the stations on an r-tree for quick lookups
        geometries = [shape(s["geometry"]) for s in self.stations]
        self.stations_index = {
            id(g): self.stations[i] for i, g in enumerate(geometries)
        }
        self.stations_rtree = STRtree(geometries)

    def get_nearest_station(self, geometry):
        """Get the nearest station to a given geometry, using the RTree index

        Arguments:
            geometry {dict} -- Geometry

        Returns:
            dict -- SiAR station
        """
        nearest = self.stations_rtree.nearest(geometry)
        if nearest is None:
            # No station "nearest"?
            # Maybe the cache has been flushed, let's re-fetch the stations before failing
            self.load_stations()
            nearest = self.stations_rtree.nearest(geometry)
        return self.stations_index[id(nearest)]

    def fetch_stations(self):
        """Retrieve the list of all weather stations from the SiAR API
        and update the redis cache

        Returns:
            list -- List of SiAR stations
        """
        response = requests.get(
            self.api_stations_url, params={"ClaveAPI": self.api_key}
        )
        response = response.json()

        if "Datos" not in response:
            raise Exception('INVALID_REPLY_FROM_DISTANT_SERVICE')

        stations = [
            {
                "id": item["Codigo"],
                "name": item["Estacion"],
                "valid_from": self._fecha_horamin_to_datetime(
                    item["Fecha_Instalacion"], 0
                ),
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        self._lonlat_str_to_float(item["Longitud"]),
                        self._lonlat_str_to_float(item["Latitud"]),
                    ],
                    "crs": {
                        "type": "name",
                        "properties": {"name": f"EPSG:{self.api_srid}"},
                    },
                },
                "source": item,
            }
            for item in response["Datos"]
        ]
        self.cache.write_stations(stations)
        return stations

    def get_hourly_observations_for_station(self, station_id, date_from, date_to):
        """Get hourly SiAR observation for a given station and a given
        date range, either from the cache or from the API

        Arguments:
            station_id {str} -- SiAR ID of station
            date_from {date} -- Start of date range
            date_to {date} -- End of date range (included)

        Returns:
            list -- List of samples
        """
        date_from = self.tz.localize(date_from)
        date_to = self.tz.localize(date_to)
        station = self.cache.get_station(station_id)
        to_fetch, in_cache = self.cache.get_date_ranges_to_fetch(
            self.cache.hourly_observations_prefix, station_id, date_from, date_to
        )

        all_samples = []

        for date_range in to_fetch:
            response = requests.get(
                self.api_hourly_observations_url,
                params={
                    "Id": station_id,
                    "ClaveAPI": self.api_key,
                    "FechaInicial": date_range[0].strftime("%Y-%m-%d"),
                    "FechaFinal": date_range[1].strftime("%Y-%m-%d"),
                },
            )
            response = response.json()

            if "Datos" not in response:
                continue

            samples_fetched = [
                self._hourly_item_to_sample(item, station) for item in response["Datos"]
            ]
            self.cache.write_samples(
                self.cache.hourly_observations_prefix,
                station_id,
                date_from,
                date_to,
                samples_fetched,
            )
            all_samples += samples_fetched

        # Query the cache
        for date_in_cache in in_cache:
            samples_in_cache = self.cache.get_samples(
                self.cache.hourly_observations_prefix,
                station_id,
                date_in_cache,
                date_in_cache,
            )
            all_samples += samples_in_cache[0]

        # Sort all the samples by valid_from field
        all_samples = sorted(all_samples, key=lambda s: s["valid_from"])

        return all_samples

    def get_daily_observations_for_station(self, station_id, date_from, date_to):
        """Get daily SiAR observation for a given station and a given
        date range, either from the cache or from the API

        Arguments:
            station_id {str} -- SiAR ID of station
            date_from {date} -- Start of date range
            date_to {date} -- End of date range (included)

        Returns:
            list -- List of samples
        """
        station = self.cache.get_station(station_id)

        # If the station does not exist, then maybe the cache has been flushed
        # in the meantime, so reload stations and retry
        if station is None:
            self.load_stations()
            station = self.cache.get_station(station_id)

        # Still does not exist, fail with no observations
        if station is None:
            return []

        # Compute the ranges to fetch from the API and from the cache,
        # given the requested interval and the current data in the cache for this station
        to_fetch, in_cache = self.cache.get_date_ranges_to_fetch(
            self.cache.daily_observation_prefix, station_id, date_from, date_to
        )

        # Query the SiAR service and parse the response
        all_samples = []
        for date_range in to_fetch:
            response = requests.get(
                self.api_daily_observation_url,
                params={
                    "Id": station_id,
                    "ClaveAPI": self.api_key,
                    "FechaInicial": date_range[0].strftime("%Y-%m-%d"),
                    "FechaFinal": date_range[1].strftime("%Y-%m-%d"),
                },
            )
            response = response.json()

            if "Datos" not in response:
                continue

            samples_fetched = [
                self._daily_item_to_sample(item, station) for item in response["Datos"]
            ]
            self.cache.write_samples(
                self.cache.daily_observation_prefix,
                station_id,
                date_from,
                date_to,
                samples_fetched,
            )
            all_samples += samples_fetched

        # Query the cache
        for date_in_cache in in_cache:
            samples_in_cache = self.cache.get_samples(
                self.cache.daily_observation_prefix,
                station_id,
                date_in_cache,
                date_in_cache,
            )
            all_samples += samples_in_cache[0]

        # Sort all the samples by valid_from field
        all_samples = sorted(all_samples, key=lambda s: s["valid_from"])

        return all_samples

    def _hourly_item_to_sample(self, item, station):
        """Map an observation a returned by the SiAR API to the FaST GraphQL
        weather schema

        Arguments:
            item {dict} -- Observation from SiAR
            station {dict} -- Weather station

        Returns:
            dict -- Weather GraphQL node
        """
        valid_to = self._fecha_horamin_to_datetime(item["Fecha"], item["HoraMin"])
        valid_from = valid_to - timedelta(minutes=30)
        sample = {
            "id": f'{station["id"]}|{valid_to.strftime("%Y-%m-%dT%H:%M:%S")}',
            "location": station,
            "origin": {
                "name": "SiAR | Sistema de Información Agroclimática para el Regadío",
                "url": "https://www.siar.es",
                "language": "es-es",
            },
            "valid_from": valid_from,
            "valid_to": valid_to,
            "sample_type": "observation",
            "interval_type": "half_hour",
        }
        sample["temperature"] = item.get("TempMedia", None)
        if item.get("HumedadMedia", None):
            sample["humidity"] = item.get("HumedadMedia", None) / 100
        sample["wind_speed"] = item.get("VelViento", None)
        sample["wind_bearing"] = item.get("DirViento", None)
        sample["precipitation_intensity"] = item.get("Precipitacion", None)
        sample["soil_temperatures"] = []
        if item.get("TempSuelo1", None):
            sample["soil_temperatures"] += [
                {"depth": 0.1, "temperature": item.get("TempSuelo1", None)}
            ]
        if item.get("TempSuelo2", None):
            sample["soil_temperatures"] += [
                {"depth": 0.3, "temperature": item.get("TempSuelo2", None)}
            ]
        sample["source"] = item

        return sample

    def _daily_item_to_sample(self, item, station):
        """Map an observation a returned by the SiAR API to the FaST GraphQL
        weather schema

        Arguments:
            item {dict} -- Observation from SiAR
            station {dict} -- Weather station

        Returns:
            dict -- Weather GraphQL node
        """
        valid_to = self._fecha_horamin_to_datetime(item["Fecha"], 0)
        valid_from = valid_to - timedelta(days=1)
        sample = {
            "id": f'{station["id"]}|{valid_to.strftime("%Y-%m-%dT%H:%M:%S")}',
            "location": station,
            "origin": {
                "name": "SiAR | Sistema de Información Agroclimática para el Regadío",
                "url": "https://www.siar.es",
                "language": "es-es",
            },
            "valid_from": valid_from,
            "valid_to": valid_to,
            "sample_type": "observation",
            "interval_type": "day",
        }
        sample["precipitation_intensity"] = item.get("Precipitacion", None)
        sample["soil_temperatures"] = []
        if item.get("TempSuelo1", None):
            sample["soil_temperatures"] += [
                {"depth": 0.1, "temperature": item.get("TempSuelo1", None)}
            ]
        if item.get("TempSuelo2", None):
            sample["soil_temperatures"] += [
                {"depth": 0.3, "temperature": item.get("TempSuelo2", None)}
            ]
        sample["temperature"] = item.get("TempMedia", None)
        sample["temperature_min"] = item.get("TempMin", None)
        if item.get("HorMinTempMin", None):
            sample["temperature_min_at"] = self._fecha_horamin_to_datetime(
                item["Fecha"], item["HorMinTempMin"]
            )
        sample["temperature_max"] = item.get("TempMax", None)
        if item.get("HorMinTempMax", None):
            sample["temperature_max_at"] = self._fecha_horamin_to_datetime(
                item["Fecha"], item["HorMinTempMax"]
            )
        sample["humidity"] = item.get("HumedadMedia", None)
        sample["humidity_min"] = item.get("HumedadMin", None)
        if item.get("HorMinHumMin", None):
            sample["humidity_min_at"] = self._fecha_horamin_to_datetime(
                item["Fecha"], item["HorMinHumMin"]
            )
        sample["humidity_max"] = item.get("HumedadMax", None)
        if item.get("HorMinHumMax", None):
            sample["humidity_max_at"] = self._fecha_horamin_to_datetime(
                item["Fecha"], item["HorMinHumMax"]
            )
        sample["wind_speed"] = item.get("VelViento", None)
        sample["wind_speed_max"] = item.get("VelVientoMax", None)
        if item.get("HorMinVelMax", None):
            sample["wind_speed_max_at"] = self._fecha_horamin_to_datetime(
                item["Fecha"], item["HorMinVelMax"]
            )
        sample["wind_speed_max_bearing"] = item.get("DirVientoVelMax", None)
        sample["wind_bearing"] = item.get("DirViento", None)
        # Convert from MJ/m2 total per day to average W/m2
        sample["radiation"] = item.get("Radiacion", None) * 1000000 / (24 * 60 * 60)
        sample["source"] = item

        return sample

    @staticmethod
    def _lonlat_str_to_float(str):
        """Convert latitude or longitude strings from SiAR to decimal degrees

        Arguments:
            str {str} -- Latitude or longitude in SiAR string format

        Returns:
            float -- Latitude or longitude in decimal degrees
        """
        hemisphere = -1.0 if str[-1:] in ["W", "S"] else 1.0
        decimals = float(str[-4:-1])
        seconds = float(str[-6:-4])
        minutes = float(str[-8:-6])
        degrees = float(str[:-8])
        return hemisphere * (
            degrees + (minutes + (seconds + decimals / 1000) / 60) / 60
        )

    def _fecha_horamin_to_datetime(self, fecha, horamin):
        """Convert the date/time format of SiAR to datetime object

        Example:
            fecha = "2020-04-10T00:00:00"
            horamin = 430
            --> return = datetime(2020, 4, 10, 4, 30, 0)

        Arguments:
            fecha {str} -- Date of the day
            horamin {int} -- Hour and minutes

        Returns:
            datetime -- Combination of date, hour and minutes
        """
        datetime_no_minutes = self.tz.localize(
            datetime.strptime(fecha, "%Y-%m-%dT%H:%M:%S")
        ).date()
        hours = horamin // 100
        minutes = ((horamin / 100) % 1) * 100
        return datetime_no_minutes + timedelta(hours=hours, minutes=minutes)


siar_cache = SiARCache()
siar_client = SiARClient(cache=siar_cache)
