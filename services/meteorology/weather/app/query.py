import os
import json
from datetime import datetime, timedelta

import graphene
import pytz
from shapely.geometry import shape

from fastplatform_graphql.types.geojson import Geometry
from fastplatform_graphql.types.weather import Sample, Alert, IntervalType

from app.external import aemet, siar


TIMEZONE = pytz.timezone(os.environ.get("TIMEZONE", "Europe/Madrid"))


class Weather(graphene.ObjectType):

    # Describe capabilities
    alerts_implemented = graphene.Boolean(default_value=True)
    forecasts_implemented = graphene.Boolean(default_value=True)
    observations_implemented = graphene.Boolean(default_value=True)

    forecasts_interval_types = graphene.List(IntervalType)
    observations_interval_types = graphene.List(IntervalType)

    def resolve_forecasts_interval_types(self, info):
        return [IntervalType.hour, IntervalType.day]

    def resolve_observations_interval_types(self, info):
        return [IntervalType.day]

    observations = graphene.List(
        Sample,
        geometry=graphene.Argument(graphene.String, required=True),
        date_from=graphene.Argument(graphene.Date, required=False),
        date_to=graphene.Argument(graphene.Date, required=False),
        interval_type=graphene.Argument(IntervalType, required=True),
    )
    forecasts = graphene.List(
        Sample,
        geometry=graphene.Argument(graphene.String, required=True),
        interval_type=graphene.Argument(IntervalType, required=True),
    )
    alerts = graphene.List(Alert, geometry=graphene.Argument(graphene.String, required=True))

    def resolve_forecasts(self, info, geometry, interval_type):
        """Resolver for `forecasts` node

        Arguments:
            info {object} -- Context as forwarded by graphene
            geometry {str} -- GeoJSON geometry, as a string

        Returns:
            list -- List of weather Samples
        """
        if interval_type not in [IntervalType.hour, IntervalType.day]:
            return None
        
        geometry = json.loads(geometry)
        municipio = aemet.aemet_client.get_max_intersection_from_geometry(
            "municipios", geometry
        )
        if municipio is None:
            return []
        samples = aemet.aemet_client.get_forecast_for_municipio(municipio, interval_type)

        return [Sample(**s) for s in samples]

    def resolve_observations(self, info, geometry, interval_type, date_from, date_to):
        """Resolver for `observations` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a __geo_interface__
            date_from {date} -- [description]
            date_to {str} -- [description]

        Returns:
            list -- List of weather Samples
        """
        if not date_to:
            date_to = datetime.now(tz=TIMEZONE).date()
        if not date_from:
            date_from = (datetime.now(tz=TIMEZONE) - timedelta(days=7)).date()

        geometry = json.loads(geometry)

        station = siar.siar_client.get_nearest_station(shape(geometry))
        if interval_type == IntervalType.day:
            samples = siar.siar_client.get_daily_observations_for_station(
                station["id"], date_from, date_to
            )
            return [Sample(**s) for s in samples]
        else:
            return None

    def resolve_alerts(self, info, geometry):
        geometry = json.loads(geometry)
        alerts = aemet.aemet_client.get_alerts_for_geometry(geometry)
        return [Alert(**a) for a in alerts]

    class Meta:
        name = "weather"


class Query(graphene.ObjectType):
    weather = graphene.Field(Weather)

    def resolve_weather(self, info):
        return Weather()
