import graphene
import logging

from fastapi import FastAPI
from starlette.graphql import GraphQLApp
from datetime import date

from app.external import aemet, siar

# from app.external.siar import setup_stations_table
from app.query import Query
from app.settings import config

from shapely.geometry import shape


app = FastAPI()

# GraphQL root
app.add_route(
    "/graphql",
    GraphQLApp(
        schema=graphene.Schema(query=Query, auto_camelcase=False)
    ),
)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
def startup():
    logger.debug(config)
    aemet.aemet_client.load_layers()
    siar.siar_client.load_stations()


@app.on_event("shutdown")
def shutdown():
    pass
